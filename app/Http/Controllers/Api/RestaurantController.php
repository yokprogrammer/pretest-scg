<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RestaurantController extends Controller
{
    public function index()
    {
        try {
            $uri = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=13.8234847,100.4906102&radius=5000&type=restaurant&keyword=bangsue&&key=AIzaSyDfeSc7kRuhm_txbcXq74HZ1YdMvvy9m9M';

            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $uri);

            $response->getStatusCode(); # 200
            $response->getHeaderLine('content-type', 'application/json'); # 'application/json; charset=utf8'
            return $response->getBody();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
}
