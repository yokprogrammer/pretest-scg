<p align="center"><img src="https://cdn-images-1.medium.com/max/1200/1*FPKwlbwrWYmQ49FmMXu4Ug.png" width="150"></p>

# PRETEST Laravel + Vue Js For SCG

> Example 5.7, news features to development

## Download
First, clone project:
``` bash
# clone
git clone https://gitlab.com/yokprogrammer/pretest-scg

# Access project
cd pretest-scg
```

## Config

``` bash
# Install dependencies
composer install

# Create file .env
.env

# Generate key
php artisan key:generate

# Create Server
php artisan serve

# Access project
http://localhost:8080

# Install npm
npm install

# Install cross-env
npm install --global cross-env
npm install --no-bin-links
npm install vue2-google-maps

# Run vue
npm run watch
```